#extrutura da tabela do banco que será convertida em formato sql
from app import db

#class pessoa que erdar de db.model mas que esta class seja convertida para modelo de dados
class  Pessoa(db.Model):
	#o nome da tabela que vai ser criada no banco de dados, tabela de pessoas
	__tablename__='pessoas'
	#variaveis serao criada colunas
	#Para criar um coluna:
	#tipo interger da colunas e chave primaria e incrementario
	id=db.Column(db.Integer,primary_key=True)
	#50 é o caracteres da string,e o nullable=False é para ser um campo obrigatorio
	nome=db.Column(db.String(50),nullable=False)
	idade=db.Column(db.Integer)
	#sexo com (1) apenas 1 carater Masculino ou Femenino (M/F)
	sexo=db.Column(db.String(1))
	salario=db.Column(db.Float)

	#valores padrão onde as variaveis vao que receber o valor dos parametros
	#caso nao seja passado durante a incricao
	def __init__(self, nome='Anônimo',idade=18,sexo='M',salario=1039):
		self.nome=nome
		self.idade=idade
		self.sexo=sexo
		self.salario=salario

	def __repr__(self):
		#Para visualizar a descriação pessoa no terminal. para facilitar o processo de debug no terminal
		return '<Pessoa %r>' % self.nome






