#bibliteca flask
from flask import Flask
#biblioteca flask para o recurso sqlite
from flask_sqlalchemy import SQLAlchemy
#para gerenciamento dos scripts
from flask_script import Manager
#Extenção que lida com migracoes do sql
from flask_migrate import Migrate, MigrateCommand

#variavel app para representar o firmwork flash
app = Flask(__name__)
#carrega arquivo de configuraçao / mesm nome do arquivo de configuração do projecto sem .py
app.config.from_object('config')
#usa aplicativo flask para construir e gerenciar o banco de dados db
db = SQLAlchemy(app)
#instanciar a class migrate, migra a estrutura de dados em Flask para sql
migrate = Migrate(app, db)
#gerenciar os comandos do banco de dados por meio do comando principal bd
manager = Manager(app)
#o nome 'db' é o que está a ser definido na execução dos comandos.txt
manager.add_command('db', MigrateCommand)

#precisa se de declarar o default agora para utilizar as definições atras escritas
#import o default da pasta controllers da pasta app
from app.controllers import default