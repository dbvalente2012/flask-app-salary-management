#controlo de diretorias
import os.path 
#diretorio base para armazenir o endereco  do diretorio do terminar do arquivo, arquivo actual
#captura diretorio absoluto
basedir = os.path.abspath(os.path.dirname(__file__))

#variavel configuraçao, projecto em modo debug
DEBUG=True
#variavel armazena o endereço do arquivo de dados sqlite
#escreve num arquivo fisico em formato sqlite
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'banco.db')
#Aceita modificações no banco de dados sem qualquer tipo de restrição
SQLALCHEMY_TRACK_MODIFICATIONS = True
